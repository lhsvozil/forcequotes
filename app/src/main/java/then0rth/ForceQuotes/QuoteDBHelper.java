package then0rth.ForceQuotes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by nthor on 15.02.2016.
 */
public class QuoteDBHelper extends SQLiteOpenHelper {

    //SQLiteDatabase m_db;
    public QuoteDBHelper(Context c){
        super(c, "QuoteDb", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("DB help", "on create pre-exec");
        db.execSQL("CREATE TABLE history (quoteNum INT UNIQUE NOT NULL, time_used TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL); " +
                "CREATE TRIGGER limit_history_count AFTER INSERT ON history " +
                "BEGIN " +
                " delete from history where quoteNum not in (SELECT quote from history order by time_used desc limit 30); " +
                "END; ");
        Log.i("DB help", "after createc");
    }
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {}

    public boolean wasThisParticularQuoteUsedRecently(int quoteNum)
    {
        Cursor cur = this.getReadableDatabase().query("history", new String[]{"quoteNum"}, "quoteNum=?", new String[]{Integer.toString(quoteNum)}, null, null, null);
        boolean value_that_will_be_returned = (cur.getCount() > 0);
        cur.close();
        return value_that_will_be_returned;
    }

    public void iHaveJustUsedAQuote(int quoteNum)
    {
        ContentValues insertValues = new ContentValues();
        insertValues.put("quoteNum", quoteNum);
        this.getWritableDatabase().insert("history", null, insertValues);
    }
}

