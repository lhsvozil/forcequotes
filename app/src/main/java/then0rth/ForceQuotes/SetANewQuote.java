package then0rth.ForceQuotes;

import android.app.Application;
import android.app.IntentService;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.util.Random;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SetANewQuote extends IntentService {

    static final String ACTION_GENERATE = "generovani noveho wallpaperu";
    static final String ACTION_RESIZE = "zmena velikosti";
    SharedPreferences preferences;
    WallpaperManager manager;

    private String[] quotes =
            //region quotes
            {"Great leaders inspire greatness in others.",
                    "Belief is not a matter of choice, but of conviction.",
                    "Easy is the path to wisdom for those not blinded by ego.",
                    "Easy is the path to wisdom for those not blinded by themselves.",
                    "A plan is only as good as those who see it through.",
                    "The best confidence builder is experience.",
                    "Trust in your friends, and they'll have reason to trust in you.",
                    "You hold onto friends by keeping your heart a little softer than your head.",
                    "Heroes are made by the times.",
                    "Ignore your instincts at your peril.",
                    "Most powerful is he who controls his own power.",
                    "The winding path to peace is always a worthy one, regardless of how many turns it takes.",
                    "Fail with honor rather than succeed by fraud.",
                    "Greed and fear of loss are the roots that lead to the tree of evil.",
                    "When surrounded by war, one must eventually choose a side.",
                    "Arrogance diminishes wisdom.",
                    "Truth enlightens the mind, but won't always bring happiness to your heart.",
                    "Fear is a disease; hope is its only cure.",
                    "A single chance is a galaxy of hope.",
                    "It is a rough road that leads to the heights of greatness.",
                    "The costs of war can never be truly accounted for.",
                    "Compromise is a virtue to be cultivated, not a weakness to be despised.",
                    "A secret shared is a trust formed.",
                    "A lesson learned is a lesson earned.",
                    "Overconfidence is the most dangerous form of carelessness.",
                    "The first step to correcting a mistake is patience.",
                    "A true heart should never be doubted.",
                    "Believe in yourself or no one else will.",
                    "No gift is more precious than trust.",
                    "Sometimes, accepting help is harder than offering it.",
                    "Attachment is not compassion",
                    "For everything you gain, you lose something else.",
                    "It is the quest for honor that makes one honorable.",
                    "Easy isn't always simple.",
                    "If you ignore the past, you jeopardize your future.",
                    "Fear not for the future, weep not for the past.",
                    "In war, truth is the first casualty.",
                    "Searching for the truth is easy. Accepting the truth is hard.",
                    "A wise leader knows when to follow.",
                    "Courage makes heroes, but trust builds friendship.",
                    "Choose what is right, not what is easy",
                    "The most dangerous beast is the beast within.",
                    "Who my father was matters less than my memory of him.",
                    "Adversity is friendship's truest test.",
                    "Revenge is a confession of pain.",
                    "Brothers in arms are brothers for life.",
                    "Fighting a war tests a soldier's skills, defending his home tests a soldier's heart.",
                    "Where there's a will, there's a way.",
                    "A child stolen is a lost hope.",
                    "The challenge of hope is to overcome corruption.",
                    "Those who enforce the law must obey the law.",
                    "The future has many paths -- choose wisely.",
                    "A failure in planning is a plan for failure.",
                    "Love comes in all shapes and sizes.",
                    "Fear is a great motivator.",
                    "Truth can strike down the specter of fear",
                    "The swiftest path to destruction is through vengeance.",
                    "Evil is not born, it is taught.",
                    "The path to evil may bring great power, but not loyalty.",
                    "Balance is found in the one who faces his guilt.",
                    "He who surrenders hope, surrenders life.",
                    "He who seeks to control fate shall never find peace.",
                    "Adaptation is the key to survival.",
                    "Anything that can go wrong will.",
                    "Without honor, victory is hollow.",
                    "Without humility, courage is a dangerous game.",
                    "A great student is what the teacher hopes to be.",
                    "When destiny calls, the chosen have no choice.",
                    "Only through fire is a strong sword forged.",
                    "Crowns are inherited, kingdoms are earned.",
                    "Who a person truly is cannot be seen with the eye.",
                    "Understanding is honoring the truth beneath the surface.",
                    "Who's the more foolish, the fool or the fool who follows him?",
                    "The first step toward loyalty is trust.",
                    "The path of ignorance is guided by fear.",
                    "The wise man leads, the strong man follows.",
                    "Our actions define our legacy.",
                    "Where we are going always reflects where we came from.",
                    "Those who enslave others inevitably become slaves themselves.",
                    "Great hope can come from small sacrifices.",
                    "Friendship shows us who we really are.",
                    "All warfare is based on deception.",
                    "Keep your friends close, but keep your enemies closer.",
                    "The strong survive, the noble overcome.",
                    "Trust is the greatest of gifts, but it must be earned.",
                    "One must let go of the past to hold onto the future.",
                    "Who we are never changes, who we think we are does.",
                    "A fallen enemy may rise again, but the reconciled one is truly vanquished.",
                    "The enemy of my enemy is my friend.",
                    "Strength in character can defeat strength in numbers.",
                    "Fear is a malleable weapon.",
                    "To seek something is to believe in its possibility.",
                    "Struggles often begin and end with the truth.",
                    "Disobedience is a demand for change.",
                    "He who faces himself, finds himself.",
                    "The young are often underestimated.",
                    "When we rescue others, we rescue ourselves.",
                    "Choose your enemies wisely, as they may be your last hope.",
                    "Humility is the only defense against humiliation.",
                    "When all seems hopeless, a true hero gives hope.",
                    "A soldier's most powerful weapon is courage.",
                    "You must trust in others or success is impossible.",
                    "One vision can have many interpretations.",
                    "Alliances can stall true intentions.",
                    "Morality separates heroes from villains.",
                    "Sometimes even the smallest doubt can shake the greatest belief.",
                    "Courage begins by trusting oneself.",
                    "Never become desperate enough to trust the untrustworthy.",
                    "Never give up hope, no matter how dark things seem.",
                    "Facing all that you fear will free you from yourself.",
                    "Death is just the beginning.",
                    "Madness can sometimes be the path to truth.",
                    "What is lost is often found.",
                    "Wisdom is born in fools as well as wise men.",
                    "Deceit is the weapon of greed.",
                    "Jealousy is the path to chaos. ",
                    "To love is to trust. To trust is to believe. ",
                    "The popular belief isn't always the correct one. ",
                    "The wise benefit from a second opinion.",
                    "Without darkness there can be no light.",
                    "When in doubt, go to the source. ",
                    "The truth about yourself is the hardest to accept.",
                    "A Jedi uses the Force for knowledge and defense, never for attack.",
                    "Anger, fear, aggression. The dark side of the Force are they.",
                    "Once you start down the dark path, forever will it dominate your destiny.",
                    "Fear is the path to the dark side. Fear leads to anger. Anger leads to hate. Hate leads to suffering. ",
                    "Attachment leads to jealousy. The shadow of greed that is.",
                    "Train yourself to let go of everything you fear to lose. ",
                    "To answer power with power, the Jedi way this is not. In this war, a danger there is of losing who we are. ",
                    "If so powerful you are, why leave?",
                    "Oh! Great warrior. Wars not make one great.",
                    "When you look at the dark side, careful you must be ... for the dark side looks back",
                    "If no mistake have you made, yet losing you are... a different game you should play.",
                    "Will he finish what he begins?",
                    "Old sins cast long shadows."};
    //endregion

    public SetANewQuote()
    {
        super("SetANewQuote");
    }

    private Bitmap make_multiline_map(String whole_text)
    {
        int textSize = preferences.getInt("text size", 35);

        String[] words = whole_text.split(" ");
        int chars_per_line = manager.getDesiredMinimumWidth() / textSize;
        String[] lines = new String[whole_text.length() / chars_per_line + 5];

        int curLine =0;
        String add = "";
        lines[0] = "";
        for (String word : words) {
            if (lines[curLine].length() + word.length() > chars_per_line)
            {
                curLine++;
                lines[curLine] = "";
                add = "";
            }
            else
                add = " ";

            lines[curLine] += add + word;

        }

        return make_map(lines);
    }

    private Bitmap make_map(String[] split_text)
    {
        int textSize = preferences.getInt("text size", 35);
        float shadow_blur = preferences.getFloat("shadow blur", 1);
        float shadow_x = preferences.getFloat("shadow x offset", 1);
        float shadow_y = preferences.getFloat("shadow y offset", 1);
        //Log.i("Preference['text size']", Integer.toString(textSize));

        int lines = 0;
        for (String line : split_text)
            if (line != null)
                lines++;

        int width = manager.getDesiredMinimumWidth();
        int height = manager.getDesiredMinimumHeight();
        float x = width / 2;
        float y = height / 2 - lines * textSize / 2 + textSize;
        //Log.i("Rozmery", String.format("width: %d | height: %d", width, height));

        Paint paint = new Paint();
        paint.setShadowLayer (shadow_blur, shadow_x, shadow_y, Color.WHITE);
        paint.setTextSize(textSize);
        paint.setColor(Color.BLUE);
        paint.setTextAlign(Paint.Align.CENTER);

        Bitmap img = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(img);

        int line_off = 0;
        for (String line : split_text)
            if (line != null)
                canvas.drawText(line, x, y + (line_off++ * textSize), paint);

        return img;
    }

    private String get_quote()
    {
        //SQLiteDatabase db = openOrCreateDatabase("QuoteDB", MODE_PRIVATE, null);
        QuoteDBHelper helper = new QuoteDBHelper(getApplicationContext());


        SharedPreferences.Editor preferenceEd = preferences.edit();
        Random rand = new Random();

        int qn;
        do
            qn = rand.nextInt(quotes.length);
        while (helper.wasThisParticularQuoteUsedRecently(qn));

        preferenceEd.putInt("quoteNum", qn);
        preferenceEd.apply();

        helper.iHaveJustUsedAQuote(qn);
        helper.close();
        return quotes[qn];
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            Log.i("alarm", "alarm tick = on handle intend");
            preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            manager = WallpaperManager.getInstance(this);

            Log.v("intent action", intent.getAction());
            if (intent.getAction().equals(ACTION_GENERATE)) {
                Log.d("OnRun", "event occurred, changing wallpaper");

                String quote = get_quote();
                try {
                    manager.setBitmap(make_multiline_map(quote));
                } catch (IOException e) {
                    Log.e("IOException", "IO Exception during WallpaperManager.setBitmap");
                }
            } else if (intent.getAction().equals(ACTION_RESIZE)){
                Log.d("OnRun", "event occurred, resizing wallpaper");

                String quote = quotes[preferences.getInt("quoteNum",0)];

                try {
                    manager.setBitmap(make_multiline_map(quote));
                } catch (IOException e) {
                    Log.e("IOException", "IO Exception during WallpaperManager.setBitmap");
                }
            }

        }
    }


}
