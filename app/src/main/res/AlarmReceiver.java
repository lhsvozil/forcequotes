/**
 * Created by nthor on 07.01.2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private final String SOMEACTION = "packagename.ACTION"; //packagename is com.whatever.www

    @Override
    public void onReceive(Context context, Intent intent) {
        Time now = new Time();
        now.setToNow();
        String time = FileHandler.timeFormat(now);

        String action = intent.getAction();
        if (SOMEACTION.equals(action)) {
            // here you call a service etc.
        }
    }
}